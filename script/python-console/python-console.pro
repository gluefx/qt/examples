TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /Library/Frameworks/Python.framework/Versions/3.8/include/python3.8
LIBS += -L/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/config-3.8-darwin -lpython3.8

SOURCES += \
        main.cpp

DISTFILES += \
    main.py

#QMAKE_POST_LINK += $$quote(cp -rf $${PWD}/main.py $${OUT_PWD})
#QMAKE_POST_LINK += cp -rf $${PWD}/main.py $${OUT_PWD}
#QMAKE_PRE_LINK += echo $${PWD}
#QMAKE_POST_LINK += echo "QMAKE_POST_LINK"
#QMAKE_PRE_LINK += $$quote(echo hello)

HEADERS += \
    custom1.h \
    custom2.h \
    custom3.h \
    custom4.h \
    emb.h \
    sublist.h
