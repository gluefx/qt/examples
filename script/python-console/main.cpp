#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <iostream>
#include <stdio.h>
#include "emb.h"
//#include "custom1.h"
//#include "custom2.h"
//#include "custom3.h"
//#include "custom4.h"
#include "sublist.h"

using namespace std;

int
main(int argc, char *argv[])
{
    cout << argc << endl;
    wchar_t *program = Py_DecodeLocale(argv[0], nullptr);
    if (program == nullptr) {
        fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
        exit(1);
    }
    Py_SetProgramName(program);  /* optional but recommended */

    numargs = argc;
//    PyImport_AppendInittab("emb", &PyInit_emb);
//    PyImport_AppendInittab("custom", &PyInit_custom);
//    PyImport_AppendInittab("custom2", &PyInit_custom2);
//    PyImport_AppendInittab("custom3", &PyInit_custom3);
//    PyImport_AppendInittab("custom4", &PyInit_custom4);
    PyImport_AppendInittab("sublist", &PyInit_sublist);

    Py_Initialize();
//    PyRun_SimpleString(
//        "from time import time,ctime\n"
//        "print('Today is', ctime(time()))\n"
//    );

    const char *filename = "../python-console/main.py";
    FILE *fp = fopen(filename, "r");
    PyRun_SimpleFile(fp, filename);


    if (Py_FinalizeEx() < 0) {
        exit(120);
    }
    PyMem_RawFree(program);
    return 0;
}
