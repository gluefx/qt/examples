/*
 * https://docs.python.org/3/extending/embedding.html#extending-embedded-python
 * 1.4. Extending Embedded Python
 * access to functionality from the application itself
*/

#ifndef EMB_H
#define EMB_H
#include <Python.h>

static int numargs=0;

/* Return the number of arguments of the application command line */
static PyObject*
emb_numargs(PyObject *self, PyObject *args)
{
    if(!PyArg_ParseTuple(args, ":numargs"))
        return nullptr;
    return PyLong_FromLong(numargs);
}

static PyMethodDef EmbMethods[] = {
    {"numargs", emb_numargs, METH_VARARGS,
     "Return the number of arguments received by the process."},
    {nullptr, nullptr, 0, nullptr}
};

static PyModuleDef EmbModule = {
    PyModuleDef_HEAD_INIT, "emb", nullptr, -1, EmbMethods,
    nullptr, nullptr, nullptr, nullptr
};

static PyObject*
PyInit_emb(void)
{
    return PyModule_Create(&EmbModule);
}

#endif // EMB_H
