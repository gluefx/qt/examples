# This Python file uses the following encoding: utf-8

# if__name__ == "__main__":
#     pass

from time import time,ctime
print('Today is', ctime(time()))

## **** simple embed
#import emb
#print("> emb: Number of arguments", emb.numargs())

# **** custom type

## **** custom1 basic
#import custom
#mycustom = custom.Custom()
#print("> custom1: ", mycustom)

## **** custom2 member
#import custom2
#mycustom2 = custom2.Custom(first='Hello', last='World', number=123)
#print("> custom2: ", mycustom2.name())
## not constrant to be string type, fix in the next chapter
#mycustom2.first = 456
#print("> custom2: ", mycustom2.name())

### **** custom3 get set
#import custom3
#mycustom3 = custom3.Custom(first='Hello', last='World', number=123)
#print("> custom3: ", mycustom3.name())
## not constrant to be string type, fix in the next chapter
#mycustom3.first = 'new string'
#print("> custom3: ", mycustom3.name())
#print("> custom3: ", mycustom3.first)

# can not GC, fix in custom4
#>>> import custom3
#>>> class Derived(custom3.Custom): pass
#...
#>>> n = Derived()
#>>> n.some_attribute = n


# **** Subclassing
#>>> import sublist
#>>> s = sublist.SubList(range(3))
#>>> s.extend(s)
#>>> print(len(s))
#6
#>>> print(s.increment())
#1
#>>> print(s.increment())
#2

import sublist
s = sublist.SubList(range(3))
print(len(s))
s.extend(s)
print(len(s))
print(s.increment())
print(s.increment())
