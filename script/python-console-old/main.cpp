#include <iostream>
#include "Python.h"

#include "emb.h"
#include "noddy2.h"
#include "shoddy.h"
#include "windowPy.h"

using namespace std;

int main(int argc, char *argv[])
{
//	cout << "Hello World!" << endl;

	wchar_t *program = Py_DecodeLocale(argv[0], NULL);
	Py_SetProgramName(program);  /* optional but recommended */

	numargs = argc;

	PyImport_AppendInittab("emb", &PyInit_emb);
	PyImport_AppendInittab("noddy", &PyInit_noddy);
	PyImport_AppendInittab("shoddy", &PyInit_shoddy);
	PyImport_AppendInittab("gt", &PyInit_gt);

	Py_Initialize();

	// test
//	PyRun_SimpleString(
//				"from time import time,ctime\n"
//                "print('123 Today is', ctime(time()))\n"
//				"#import emb\n"
//				"#print('Number of arguments: ', emb.numargs())\n"
//				"#print('My String: ', emb.mystr('123'))\n"
//				"import noddy\n"
//				"#mynoddy = noddy.Noddy()\n"
//				"#print(noddy)\n"
//				"#print(mynoddy)\n"
//				"mynoddy2 = noddy.Noddy(first='guan', last='xu2', number=123)\n"
//				"mynoddy2.first = 'hello'\n"
//				"print(mynoddy2.name())\n"
//				"#print(mynoddy2.number)\n"
//				"#mynoddy3 = noddy.Noddy(str.encode('guan'), str.encode('xu3'), 123)\n"
//				"#print(mynoddy3.name())\n"
//				"#mynoddy3.first = [1, 2, 3]\n"
//				"#print(mynoddy3.name())\n"
//				"#mynoddy4 = noddy.Noddy('guan', 'xu4', 123)\n"
//				"#print(mynoddy4.name())\n"
//				"#import shoddy\n"
//				"#s = shoddy.Shoddy(range(3))\n"
//				"#s.extend(s)\n"
//				"#print(len(s))\n"
//				"#print(s.increment())\n"
//				"#print(s.increment())\n"
//					   );

	// WindowPy
	PyRun_SimpleString(
				"import gt\n"
                "from time import time,ctime\n"
                "print('Today is', ctime(time()))\n"
				"win = gt.Window(color='red')\n"
                "print('py: 1. getcolor(): ' + win.getcolor())\n"
                "print('py: 1. color: ' + win.color)\n"
                "print('py: --- win.color = black')\n"
                "win.color = 'black'\n"
                "print('py: 2. getcolor(): ' + win.getcolor())\n"
                "print('py: 2. color: ' + win.color)\n"
				);


	Py_Finalize();
	PyMem_RawFree(program);

		return 0;
}
