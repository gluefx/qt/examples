#ifndef EMB_H
#define EMB_H

#include "Python.h"

static int numargs=0;

/* Return the number of arguments of the application command line */
static PyObject*
emb_numargs(PyObject *self, PyObject *args)
{
	if(!PyArg_ParseTuple(args, ":numargs"))
		return NULL;
	return PyLong_FromLong(numargs);
}

static PyObject*
emb_mystr(PyObject *self, PyObject *args)
{
	const char *inStr;
	int ok = PyArg_ParseTuple(args, "s", &inStr); /* A string */
	return PyUnicode_FromString(inStr);
}

static PyMethodDef EmbMethods[] = {
	{"numargs", emb_numargs, METH_VARARGS,
	 "Return the number of arguments received by the process."},
	{"mystr", emb_mystr, METH_VARARGS,
	 "Return the my str."},
	{NULL, NULL, 0, NULL}
};

static PyModuleDef EmbModule = {
	PyModuleDef_HEAD_INIT, "emb", NULL, -1, EmbMethods,
	NULL, NULL, NULL, NULL
};

static PyObject*
PyInit_emb(void)
{
	printf("-- PyInit_emb");
	return PyModule_Create(&EmbModule);
}

#endif // EMB_H
