#include <Python.h>
#include "structmember.h"

class Window {
public:
	void setColor(char *color)
	{
		this->_color = color;
	}
	char* color() {
		return this->_color;
	}
private:
	char *_color;
};



typedef struct {
    PyObject_HEAD
	PyObject *color; /* color */
	void *pointer;
} WindowPy;

static void
WindowPy_dealloc(WindowPy* self)
{
	Py_XDECREF(self->color);
    Py_TYPE(self)->tp_free((PyObject*)self);
}

static PyObject *
WindowPy_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
	WindowPy *self;

	self = (WindowPy *)type->tp_alloc(type, 0);
    if (self != NULL) {
		self->color = PyUnicode_FromString("");
		if (self->color == NULL) {
            Py_DECREF(self);
            return NULL;
        }

		// self->number = 0;
    }

	self->pointer = new Window();
    printf("c: window pointer1: %d\n", self->pointer);

    return (PyObject *)self;
}

static int
WindowPy_init(WindowPy *self, PyObject *args, PyObject *kwds)
{
	PyObject *color=NULL, *tmp;

	static char *kwlist[] = {"color", NULL};

	if (! PyArg_ParseTupleAndKeywords(args, kwds, "|O", kwlist,
									  &color))
        return -1;

	if (color) {
		tmp = self->color;
		Py_INCREF(color);
		self->color = color;
        Py_XDECREF(tmp);
    }

    printf("c: window pointer2: %d\n", self->pointer);

	Window *win = (Window *)(self->pointer);
    const char *c_color = PyUnicode_AsUTF8(color);
    printf("c: the color is: %s\n", c_color);

    char* new_color = new char[100];//足够长
    strcpy(new_color, c_color);
    win->setColor(new_color);

    return 0;
}


static PyMemberDef WindowPy_members[] = {
	{"color", T_OBJECT_EX, offsetof(WindowPy, color), 0,
	 "color of the window"},
    {NULL}  /* Sentinel */
};

static PyObject *
WindowPy_getcolor(WindowPy* self)
{
    if (self->color == nullptr) {
		PyErr_SetString(PyExc_AttributeError, "getcolor");
        return NULL;
    }

	Window *win = (Window *)(self->pointer);
	char *c_color = win->color();
    printf("c: WindowPy_getcolor color: %s\n", c_color);
	PyObject *color = PyUnicode_FromString(c_color);

	return PyUnicode_FromFormat("color: %S", color);
}

static PyMethodDef WindowPy_methods[] = {
	{"getcolor", (PyCFunction)WindowPy_getcolor, METH_NOARGS,
	 "Return the windows's color"
    },
    {NULL}  /* Sentinel */
};

static PyTypeObject WindowPyType = {
    PyVarObject_HEAD_INIT(NULL, 0)
	"gt.WindowPy",             /* tp_name */
	sizeof(WindowPy),             /* tp_basicsize */
    0,                         /* tp_itemsize */
	(destructor)WindowPy_dealloc, /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT |
        Py_TPFLAGS_BASETYPE,   /* tp_flags */
	"WindowPy objects",           /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
	WindowPy_methods,             /* tp_methods */
	WindowPy_members,             /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
	(initproc)WindowPy_init,      /* tp_init */
    0,                         /* tp_alloc */
	WindowPy_new,                 /* tp_new */
};

static PyModuleDef windowPyModule = {
    PyModuleDef_HEAD_INIT,
	"gt",
	"Example module that Glue Toolkit.",
    -1,
    NULL, NULL, NULL, NULL, NULL
};

PyMODINIT_FUNC
PyInit_gt(void)
{
    PyObject* m;

	if (PyType_Ready(&WindowPyType) < 0)
        return NULL;

	m = PyModule_Create(&windowPyModule);
    if (m == NULL)
        return NULL;

	Py_INCREF(&WindowPyType);
	PyModule_AddObject(m, "Window", (PyObject *)&WindowPyType);
    return m;
}
