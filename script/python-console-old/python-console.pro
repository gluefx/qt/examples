TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

CONFIG += link_pkgconfig no_keywords
#PKGCONFIG += python-3.5
CONFIG += c++11

#INCLUDEPATH += /Library/Frameworks/Python.framework/Versions/3.5/include/python3.5m
#LIBS += -L/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/config-3.5m -lpython3.5m

#INCLUDEPATH += /Library/Frameworks/Python.framework/Versions/3.7/include/python3.7m
#LIBS += -L/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/config-3.7m-darwin -lpython3.7m

INCLUDEPATH += /Library/Frameworks/Python.framework/Versions/3.8/include/python3.8
LIBS += -L/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/config-3.8-darwin -lpython3.8

HEADERS += \
    emb.h \
    noddy.h \
    noddy2.h \
    noddy3.h \
    noddy4.h \
    shoddy.h \
    windowPy.h

DISTFILES += \
    README.md
