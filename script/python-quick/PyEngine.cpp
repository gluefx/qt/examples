#include "PyEngine.h"
#include <QDebug>
#include "CustomPy.h"
#include <Python.h>
#include <string>

PyEngine::PyEngine(QObject *parent) : QObject(parent)
{

}

PyEngine::PyEngine(QObject *parent, QObject *rootObject) : QObject(parent)
{
    this->rootObject = rootObject;
    QObject::connect(rootObject, SIGNAL(runScript(QString)),
                     this, SLOT(runScript(QString)));

//    wchar_t *program = Py_DecodeLocale("prgram_name", nullptr);
//    Py_SetProgramName(program);
    PyImport_AppendInittab("custom4", &PyInit_custom4);
    Py_Initialize();

    PyRun_SimpleString(
                "import io, sys\n"
                "class StdoutCatcher(io.TextIOBase):\n"
                "    def __init__(self):\n"
                "        self.data = ''\n"
                "    def write(self, stuff):\n"
                "        self.data = self.data + stuff\n"
                "catcher = StdoutCatcher()\n"
                "sys.stdout = catcher\n"
                "print('hello world!')\n"
    );

//    PyRun_SimpleString("import sys");
    PyRun_SimpleString("from time import time,ctime");
    PyRun_SimpleString("print('Today is', ctime(time()))");

    PyRun_SimpleString("import custom4");
    PyRun_SimpleString("mycustom4 = custom4.Custom(first='Hello', last='World', number=123)");
    PyRun_SimpleString("print(\"> custom4: \", mycustom4.name())");

    flush();

    PyObject* m = PyImport_AddModule("__main__");
    PyObject* catcher = PyObject_GetAttrString(m, "catcher");
    PyObject* output = PyObject_GetAttrString(catcher, "data");
    const char *out = PyUnicode_AsUTF8(output);

    qDebug() << ">>> myoutput: " << out << endl;

//    if (Py_FinalizeEx() < 0) {
//        exit(120);
//    }
//    PyMem_RawFree(program);
}

void PyEngine::flush()
{
    PyRun_SimpleString("sys.stdout.flush()");
}

void PyEngine::runScript(const QString &script)
{
    // TODO: uff-8 support
    // UnicodeEncodeError: 'ascii' codec can't encode characters in position 0-1: ordinal not in range(128)
    const char *scriptString = script.toUtf8().constData();

//    PyRun_SimpleString(scriptString);

    // TODO: normal stdout and expression all print to same console
    PyObject* mainModule = PyImport_AddModule("__main__");
    PyObject* dict = PyModule_GetDict(mainModule);

    PyObject* evalResult = PyRun_String(scriptString , Py_eval_input, dict, dict);
    // TODO: detect evalResult type and convert to proper c type
    const char *evalString = PyUnicode_AsUTF8(evalResult);
    qDebug() << ">>> evalString: " << evalString << endl;

    flush();

    PyObject* catcher = PyObject_GetAttrString(mainModule, "catcher");
    PyObject* output = PyObject_GetAttrString(catcher, "data");
    const char *out = PyUnicode_AsUTF8(output);

    qDebug() << ">>> myoutput2: " << out << endl;

    QString returnedValue;
    QString msg = QString(out);
    QMetaObject::invokeMethod(rootObject, "showOutput",
            Q_RETURN_ARG(QString, returnedValue),
            Q_ARG(QString, msg));

    qDebug() << "QML function returned:" << returnedValue;
}
