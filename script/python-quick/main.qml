import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    signal runScript(script: string)

    function showOutput(msg: string) : string {
        console.log("Got message:", msg)
        outputText.text = msg;
        return "some return value"
    }

    ColumnLayout {
        anchors.fill: parent
        Rectangle {
            id: scriptRect
            Layout.alignment: Qt.AlignLeft
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 200
//            height: 200
//            anchors.top: parent.top
//            anchors.right: parent.right
//            anchors.left: parent.left
            border.width: 1
            border.color: "#aaa"
            TextArea {
                id: scriptText
                anchors.fill: parent
                placeholderText: qsTr("脚本")
                text: "print('hello')"
            }
        }

        Button {
            id: runButton
//            anchors.top: scriptRect.bottom
//            anchors.left: parent.left
            text: "Run"
            onClicked: window.runScript(scriptText.text)
        }

        Rectangle {
            id: outputRect
            Layout.alignment: Qt.AlignLeft
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 200
//            height: 200
//            anchors.top: runButton.bottom
//            anchors.right: parent.right
//            anchors.left: parent.left
            border.width: 1
            border.color: "#aaa"
            TextArea {
                id: outputText
                anchors.fill: parent
                placeholderText: qsTr("输出")
            }
        }
    }



//    Text {
//        objectName: "title"
//        text: "Hello"
//    }

}

//import QtQuick 2.0

//Item {
//    width: 100; height: 100
//    Text {
//        objectName: "title"
//        text: "Hello"
//    }
//}
