//#include <Python.h>
//#undef B0
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QDebug>
#include "PyEngine.h"
//#include <Python.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    QList<QObject *> objects = engine.rootObjects();
//    qDebug() << objects.size() << endl;
    QObject *rootObject = objects[0];

    PyEngine pyEngine(nullptr, rootObject);

//    QObject::connect(object, SIGNAL(runScript(QString)),
//                     &pyEngine, SLOT(runScript(QString)));

//    QObject *title = object->findChild<QObject*>("title");
//    if (title) {
//        title->setProperty("text", "World");
//    }

//    QObject *title = engine.findChild<QObject*>("title");
//    if (title) {
//        title->setProperty("text", "World");
//    }

//    QQuickView view;
//    view.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
//    view.show();
//    QObject *object = (QObject *)view.rootObject();
//    object->setProperty("width", 500);

//    QObject *title = object->findChild<QObject*>("title");
//    if (title) {
//        title->setProperty("text", "World");
//    }

    return app.exec();
}
