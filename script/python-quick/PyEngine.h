#ifndef PYMANAGER_H
#define PYMANAGER_H

#include <QObject>

class PyEngine : public QObject
{
    Q_OBJECT
public:
    explicit PyEngine(QObject *parent = nullptr);
    explicit PyEngine(QObject *parent = nullptr, QObject *rootObject = nullptr);
    void flush();
//signals:

//public slots:
// https://stackoverflow.com/questions/15078060/embedding-python-in-qt-5
// https://doc.qt.io/qt-5/signalsandslots.html#using-qt-with-3rd-party-signals-and-slots
public Q_SLOTS:
    void runScript(const QString &script);

private:
    QObject *rootObject;
};

#endif // PYMANAGER_H
