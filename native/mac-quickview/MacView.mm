#include "MacView.h"
#include <Cocoa/Cocoa.h>

void addMacView(QQuickWindow *w)
{
    NSView *view = reinterpret_cast<NSView *>(w->winId());

    CGRect  viewRect = CGRectMake(50, 50, 200, 200);
    NSView* myView = [[NSView alloc] initWithFrame:viewRect];
    [myView setBackgroundColor:[NSColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0]];

//    Mac Cocoa has no UILabel
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 30)];
//    label.backgroundColor = [UIColor yellowColor];
//    label.text = @"UILabel";

    NSTextField *label = [[NSTextField alloc] init];
//    label.editable = NO;
    label.stringValue = @"Mac View测试";
    label.frame = NSMakeRect(20, 20, 100, 40);
    [myView addSubview: label];


    NSButton *button = [[NSButton alloc]initWithFrame:CGRectMake(20, 60, 100, 40)];
    button.title = @"Button";
    button.bezelStyle = NSRoundedBezelStyle;
    [myView addSubview: button];

    [view addSubview: myView];
}
