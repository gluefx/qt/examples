#include <QGuiApplication>
//#include <QQmlApplicationEngine>
#include <QQuickView>
#include "MacView.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

//    QQmlApplicationEngine engine;
//    const QUrl url(QStringLiteral("qrc:/main.qml"));
//    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
//                     &app, [url](QObject *obj, const QUrl &objUrl) {
//        if (!obj && url == objUrl)
//            QCoreApplication::exit(-1);
//    }, Qt::QueuedConnection);
//    engine.load(url);

    QQuickView *view = new QQuickView;
//          view->setSource(QUrl::fromLocalFile("main.qml"));
          view->setSource(QUrl("qrc:/main.qml"));
          view->show();
    addMacView(view);

    return app.exec();
}
